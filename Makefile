.PHONY: $(MAKECMDGOALS)
_default: ipy

dev:
	poetry run uvicorn --reload authserv.app:app

ipy:
	poetry run ipython

db-revision:
	poetry run alembic revision -m 'throwaway' --autogenerate

db-upgrade:
	poetry run alembic upgrade head

db-seed:
	poetry run python -m lunni_backend.scripts.seed_db

db-init: db-upgrade db-seed
