from datetime import datetime, timedelta
from typing import Annotated

from fastapi import Depends, FastAPI, Form, HTTPException, Query, status
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.security import OAuth2AuthorizationCodeBearer
from jose import JWTError, jws, jwt
from starlette.datastructures import URL

from .models.token import Token, TokenData
from .models.user import User, UserInDB
from .pwd_context import pwd_context

SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30
CODE_EXPIRE_MINUTES = 5
AUTHSERV_AUDIENCE = "authserv"


fake_users_db = {
    "johndoe": {
        "username": "johndoe",
        "full_name": "John Doe",
        "email": "johndoe@example.com",
        "hashed_password": "$2b$12$EixZaYVK1fsbw1ZfbX3OXePaWxn96p36WQoeG6Lruj3vjPGga31lW",
        "disabled": False,
    },
}

oauth2_scheme = OAuth2AuthorizationCodeBearer(
    authorizationUrl="/oauth/authorize",
    tokenUrl="/oauth/token",
    scopes={"oidctest": 1},
)
app = FastAPI()


@app.get("/oauth/authorize", response_class=HTMLResponse)
def authorize(
    audience: Annotated[str, Query(description="API Identifier of the API for which you want to get an Access Token.")] = "",
    response_type: Annotated[str | None, Query(pattern="code")] = None,
    client_id: Annotated[str | None, Query()] = None,
    state: Annotated[str | None, Query()] = None,
    redirect_uri: Annotated[str, Query(description="The URL to which authserv will redirect the browser after authorization has been granted by the user.")] = "",
    scope: Annotated[str, Query(description="The scopes which you want to request authorization for. These must be separated by a space.")] = "",
):
    return """
    <form method=POST>
    <input type="text" name="username" placeholder="username">
    <button type="submit">Authorize</button>
    </form>
    """


@app.post("/oauth/authorize")
def authorize_post(
    username: Annotated[str, Form()],
    audience: Annotated[str, Query(description="API Identifier of the API for which you want to get an Access Token.")] = "",
    response_type: Annotated[str | None, Query(pattern="code")] = None,
    client_id: Annotated[str | None, Query()] = None,
    state: Annotated[str | None, Query()] = None,
    redirect_uri: Annotated[str, Query(description="The URL to which authserv will redirect the browser after authorization has been granted by the user.")] = "",
    scope: Annotated[str, Query(description="The scopes which you want to request authorization for. These must be separated by a space.")] = "",
):
    code = username
    url = URL(redirect_uri)
    url = url.include_query_params(code=code, state=state)
    return RedirectResponse(url, status_code=status.HTTP_303_SEE_OTHER)


@app.post("/oauth/access_token")
async def login_for_access_token(
    grant_type: Annotated[str, Form(description="Denotes the flow you are using", pattern="authorization_code")],
    code: Annotated[str, Form()],
    redirect_uri: Annotated[str, Form(description="Currently ignored. This is required only if it was set at the GET /authorize endpoint. The values must match.")],
):
    return {"access_token": code, "token_type": "bearer"}


@app.get("/user.json")
async def read_users_me(
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return {
        "user": {
            "uid": token,
            "name": token,
            "email": f"{token}@digital.cabinet-office.gov.uk",
            "permissions": ["signin"]
        },
    }
